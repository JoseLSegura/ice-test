.PHONY: deploy deploynode deployfiles run-checker
DEPLOYDIR=/tmp/deploy

run-checker: config
	check_ice_working --Ice.Config=config

deploy: deploynode deployfiles

deployfiles: ice_test venv/bin/check_ice_server
	mkdir -p ${DEPLOYDIR}
	cp -r $^ ${DEPLOYDIR}
	icepatch2calc ${DEPLOYDIR}
	icegridadmin --Ice.Config=locator.config -u$(shell head -1 credentials.txt) -p$(shell tail -1 credentials.txt) -e "application patch -f IceTest"

deploynode: deploynode.config /tmp/db/deploynode
	icegridnode --Ice.Config=$< &

/tmp/db/%:
	mkdir -p $@
