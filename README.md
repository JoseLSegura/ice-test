# Ice Installation Test

![Python3.5](https://img.shields.io/badge/python-3.5-blue.svg)

This little program allows to the user to check if her ZeroC installation is
working properly.

## Installation

The recommended method for installation is using pip:

`pip3 install git+https://JoseLSegura@bitbucket.org/JoseLSegura/ice-test.git`

## Running the check

This software provided 3 scripts:

* `check_ice_pymodule`: Only checks for the ZeroC Ice Python3 installation
* `check_ice_working`: Checks if the ZeroC installation works with
  your current network configuration, using a server (keep reading) to
  connect and receive an incomming connection
* `check_ice_server`: This is the counterpart of the previous
  script. Usually you don't need to run it
  
For testing your Ice installation:

1. Run `check_ice_pymodule`: it will show if you have installed ZeroC Ice Python bindings properly.
2. Run `check_ice_working`: it will try to spawn a servant. After that, it will connect to a running server and will pass it a proxy to the servant. The server will try to connect to your servant. If all the steps work, you will get a nice message.

## Configuration

As almost every Ice program, `check_ice_working` needs a configuration
file. You can find one working example in this repository, in the file
`config`.

The only 2 mandatory properties are `Adapter.Endpoints` (for
configuring the object adapter) and `Server`, that is the proxy of a
running server.
