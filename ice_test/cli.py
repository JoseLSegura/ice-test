# -*- coding: utf-8 -*-

from __future__ import print_function

import sys


def check_pymodule():
    try:
        import Ice
        print('You have Ice Python module installed')
        return 0

    except:
        print('''You don\'t have Ice Python module installed

Try installing with "pip install zeroc-ice".

If you get failures, try to isntall compilation dependencies before
running pip:

* libssl-dev
* libbz2-dev
''')
        return 1


def run_student_checker():
    from ice_test.checker_app import CheckerApp

    sys.exit(CheckerApp().main(sys.argv))


def run_student_check_server():
    from ice_test.server_app import ServerApp

    sys.exit(ServerApp().main(sys.argv))
