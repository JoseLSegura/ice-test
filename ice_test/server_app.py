# -*- coding: utf-8 -*-

import Ice

from ice_test.check import CheckI


class ServerApp(Ice.Application):
    def run(self, args):
        print('Running ZeroC Ice for Python checker SERVER side')
        adapter = self.communicator().createObjectAdapter('Adapter')
        servant = CheckI()

        identity = self.communicator().stringToIdentity(
            self.communicator().getProperties().getProperty('ServerId'))
        prx = adapter.add(servant, identity)
        print(prx)

        adapter.activate()

        self.shutdownOnInterrupt()
        self.communicator().waitForShutdown()
        
        return 0
