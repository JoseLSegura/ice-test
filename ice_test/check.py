# -*- coding: utf-8 -*-

import os.path

import Ice


dirpath = os.path.dirname(__file__)
Ice.loadSlice(os.path.join(dirpath, 'Check.ice'))

from IceTest import Check


class CheckI(Check):
    def checkClient(self, current):
        return True

    def checkServer(self, servant_prx, current):
        print(servant_prx)
        return servant_prx.check()
