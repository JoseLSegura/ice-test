module IceTest{
    interface CheckServant {
        bool check();
    };

    interface Check {
        bool checkClient();
        bool checkServer(CheckServant* servant);
    };
};
