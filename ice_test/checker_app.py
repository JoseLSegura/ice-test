# -*- coding: utf-8 -*-

import os.path

import Ice

from ice_test.check_servant import ServantI


dirname = os.path.dirname(__file__)
Ice.loadSlice(os.path.join(dirname, 'Check.ice'))

import IceTest


class CheckerApp(Ice.Application):
    def run(self, args):
        print('Running ZeroC Ice for Python checker...')
        adapter = self.communicator().createObjectAdapter('Adapter')
        servant = ServantI()

        prx = adapter.addWithUUID(servant)
        prx = IceTest.CheckServantPrx.uncheckedCast(prx)

        adapter.activate()

        server_prx = self.communicator().propertyToProxy('Server')
        server_prx = IceTest.CheckPrx.checkedCast(server_prx)
        print(server_prx)

        if server_prx.checkClient():
            print('Client side working...')

        if server_prx.checkServer(prx):
            print('Server side check working')
        
        self.shutdownOnInterrupt()
        self.communicator().waitForShutdown()
        
        return 0
