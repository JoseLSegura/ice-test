# -*- coding: utf-8 -*-

import os.path

import Ice


dirpath = os.path.dirname(__file__)
Ice.loadSlice(os.path.join(dirpath, 'Check.ice'))

from IceTest import CheckServant


class ServantI(CheckServant):
    '''
    Servant that receives the check call from remote in order
    to check if Ice is working
    '''
    def check(self, current):
        print('Check OK')
        current.adapter.getCommunicator().shutdown()
        return True
