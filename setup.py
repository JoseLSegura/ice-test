# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name='ice-test',
    packages=find_packages(),
    package_data={'': ['Check.ice']},
    include_package_data=True,
    version='0.0.1',
    description='Test Ice installation tool',
    author='Jose Luis Segura Lucas',
    author_email='josel.segura@gmx.es',
    license='GPL v3',
    entry_points={
        'console_scripts': [
            'check_ice_pymodule = ice_test.cli:check_pymodule',
            'check_ice_working = ice_test.cli:run_student_checker',
            'check_ice_server = ice_test.cli:run_student_check_server'
        ],
    }
)
